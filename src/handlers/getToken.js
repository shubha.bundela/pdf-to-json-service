import AWS from 'aws-sdk';
import { config } from './config';
import getJson from './getJson';

AWS.config.update({
  accessKeyId: config.awsAccesskeyID,
  secretAccessKey: config.awsSecretAccessKey,
  region: config.awsRegion,
});

/**
 * get Job id by pdf
 * @export
 * @param {string} fileName file name
 * @return {string} jobId string in response
 */
export default async function getToken(fileName) {
  const textract = new AWS.Textract({ apiVersion: '2018-06-27' });
  const params = {
    DocumentLocation: {
      S3Object: {
        Bucket: 'test-demo-chat',
        Name: `${fileName}.pdf`,
      },
    },
    FeatureTypes: ['TABLES', 'FORMS'],
  };

  const result = await textract.startDocumentAnalysis(params).promise();

  setTimeout(() => {
    getJson(result.JobId, fileName);
  }, 30000);

  return {
    statusCode: 200,
    body: JSON.stringify(result),
  };
}
