import AWS from 'aws-sdk';
import { config } from './config';
import mime from 'mime-types';
import getToken from './getToken';

const s3 = new AWS.S3({
  accessKeyId: config.awsAccesskeyID,
  secretAccessKey: config.awsSecretAccessKey,
  region: config.awsRegion,
});

/**
 * get PDF to JSON
 * @param {string} fileName file name
 * @param {file} file file name
 * @return {array} JSON array in response
 */
async function uploadPdf(event, context) {
  const fileContent = event.isBase64Encoded ? Buffer.from(event.body, 'base64') : event.body;
  const fileName = event.queryStringParameters.name;
  const contentType = 'application/pdf';
  const extension = contentType ? mime.extension(contentType) : '';
  const fullFileName = extension ? `${fileName}.${extension}` : fileName;

  // Upload the file to S3
  try {
    const data = await s3
      .putObject({
        Bucket: 'test-demo-chat',
        Key: fullFileName,
        Body: fileContent,
        Metadata: {},
      })
      .promise();

    console.log('Successfully uploaded file', fullFileName);
    const pdfToJson = await getToken(fileName);
    return 'Successfully uploaded';
  } catch (err) {
    console.log('Failed to upload file', fullFileName, err);
    throw err;
  }
}

export const handler = uploadPdf;
