import AWS from 'aws-sdk';
import fs from 'fs';
import { config } from './config';

AWS.config.update({
  accessKeyId: config.awsAccesskeyID,
  secretAccessKey: config.awsSecretAccessKey,
  region: config.awsRegion,
});

/**
 * get JSON by Job Id
 * @export
 * @return {array} JSON array in response
 */

export default async function getJson(jobId, fileName, token) {
  let stroreJson = [];
  const writeStream = fs.createWriteStream(`./resultJson/${fileName}.json`);
  const textract = new AWS.Textract({ apiVersion: '2018-06-27' });
  let params = {
    JobId: jobId,
    NextToken: token,
  };
  const resData = await textract.getDocumentAnalysis(params).promise();
  // pagination process
  token = resData.NextToken;
  if (token == undefined) {
    stroreJson.push(resData);
    writeStream.write(JSON.stringify(stroreJson));
    writeStream.on('finish', () => {
      console.log('wrote all data to file');
    });
    console.log('Page not found !');
  } else {
    getJson(jobId, token);
    stroreJson.push(resData);
    writeStream.write(JSON.stringify(stroreJson));
    writeStream.on('finish', () => {
      console.log('wrote all data to file');
    });
  }
}
